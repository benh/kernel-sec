From: Ben Hutchings <benh@debian.org>
To: debian-security-announce@lists.debian.org
Subject: [DSA 3237-1] linux security update

-------------------------------------------------------------------------
Debian Security Advisory DSA-3237-1                   security@debian.org
http://www.debian.org/security/                             Ben Hutchings
April 26, 2015                         http://www.debian.org/security/faq
-------------------------------------------------------------------------

Package        : linux
CVE ID         : CVE-2014-8159 CVE-2014-9715 CVE-2015-2041 CVE-2015-2042
                 CVE-2015-2150 CVE-2015-2830 CVE-2015-2922 CVE-2015-3331
                 CVE-2015-3332 CVE-2015-3339
Debian Bug     : 741667 782515 782561 782698

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2014-8159

    It was found that the Linux kernel's InfiniBand/RDMA subsystem did
    not properly sanitize input parameters while registering memory
    regions from user space via the (u)verbs API. A local user with
    access to a /dev/infiniband/uverbsX device could use this flaw to
    crash the system or, potentially, escalate their privileges on the
    system.

CVE-2014-9715

    It was found that the netfilter connection tracking subsystem used
    too small a type as an offset within each connection's data
    structure, following a bug fix in Linux 3.2.33 and 3.6.  In some
    configurations, this would lead to memory corruption and crashes
    (even without malicious traffic).  This could potentially also
    result in violation of the netfilter policy or remote code
    execution.

    This can be mitigated by disabling connection tracking accounting:
        sysctl net.netfilter.nf_conntrack_acct=0

CVE-2015-2041

    Sasha Levin discovered that the LLC subsystem exposed some variables
    as sysctls with the wrong type.  On a 64-bit kernel, this possibly
    allows privilege escalation from a process with CAP_NET_ADMIN
    capability; it also results in a trivial information leak.

CVE-2015-2042

    Sasha Levin discovered that the RDS subsystem exposed some variables
    as sysctls with the wrong type.  On a 64-bit kernel, this results in
    a trivial information leak.

CVE-2015-2150

    Jan Beulich discovered that Xen guests are currently permitted to
    modify all of the (writable) bits in the PCI command register of
    devices passed through to them.  This in particular allows them to
    disable memory and I/O decoding on the device unless the device is
    an SR-IOV virtual function, which can result in denial of service
    to the host.

CVE-2015-2830

    Andrew Lutomirski discovered that when a 64-bit task on an amd64
    kernel makes a fork(2) or clone(2) system call using int $0x80, the
    32-bit compatibility flag is set (correctly) but is not cleared on
    return.  As a result, both seccomp and audit will misinterpret the
    following system call by the task(s), possibly leading to a
    violation of security policy.

CVE-2015-2922

    Modio AB discovered that the IPv6 subsystem would process a router
    advertisement that specifies no route but only a hop limit, which
    would then be applied to the interface that received it.  This can
    result in loss of IPv6 connectivity beyond the local network.

    This may be mitigated by disabling processing of IPv6 router
    advertisements if they are not needed:
        sysctl net.ipv6.conf.default.accept_ra=0
        sysctl net.ipv6.conf.<interface>.accept_ra=0

CVE-2015-3331

    Stephan Mueller discovered that the optimised implementation of
    RFC4106 GCM for x86 processors that support AESNI miscalculated
    buffer addresses in some cases.  If an IPsec tunnel is configured to
    use this mode (also known as AES-GCM-ESP) this can lead to memory
    corruption and crashes (even without malicious traffic).  This could
    potentially also result in remote code execution.

CVE-2015-3332

    Ben Hutchings discovered that the TCP Fast Open feature regressed
    in Linux 3.16.7-ckt9, resulting in a kernel BUG when it is used.
    This can be used as a local denial of service.

CVE-2015-3339

    It was found that the execve(2) system call can race with inode
    attribute changes made by chown(2).  Although chown(2) clears the
    setuid/setgid bits of a file if it changes the respective owner ID,
    this race condition could result in execve(2) setting effective
    uid/gid to the new owner ID, a privilege escalation.

For the oldstable distribution (wheezy), these problems have been fixed
in version 3.2.68-1+deb7u1. The linux package in wheezy is not affected
by CVE-2015-3332.

For the stable distribution (jessie), these problems have been fixed in
version 3.16.7-ckt9-3~deb8u1 or earlier versions. Additionally, this
version fixes a regression in the xen-netfront driver (#782698).

For the unstable distribution (sid), these problems have been fixed in
version 3.16.7-ckt9-3 or earlier versions.  Additionally, this version
fixes a regression in the xen-netfront driver (#782698).

We recommend that you upgrade your linux packages.

Further information about Debian Security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: https://www.debian.org/security/

Mailing list: debian-security-announce@lists.debian.org
