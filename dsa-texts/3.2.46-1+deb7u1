----------------------------------------------------------------------
Debian Security Advisory DSA-2745-1                security@debian.org
http://www.debian.org/security/                           Dann Frazier
August 28, 2013                     http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux
Vulnerability  : privilege escalation/denial of service/information leak
Problem type   : local/remote
Debian-specific: no
CVE Id(s)      : CVE-2013-1059 CVE-2013-2148 CVE-2013-2164 CVE-2013-2232
                 CVE-2013-2234 CVE-2013-2237 CVE-2013-2851 CVE-2013-2852
                 CVE-2013-4162 CVE-2013-4163
Debian Bug     : 701744

Several vulnerabilities have been discovered in the Linux kernel that may lead
to a denial of service, information leak or privilege escalation. The Common
Vulnerabilities and Exposures project identifies the following problems:

CVE-2013-1059

    Chanam Park reported an issue in the Ceph distributed storage system.
    Remote users can cause a denial of service by sending a specially crafted
    auth_reply message.

CVE-2013-2148

    Dan Carpenter reported an information leak in the filesystem wide access
    notification subsystem (fanotify). Local users could gain access to
    sensitive kernel memory.

CVE-2013-2164

    Jonathan Salwan reported an information leak in the CD-ROM driver. A
    local user on a system with a malfunctioning CD-ROM drive could gain
    access to sensitive memory.

CVE-2013-2232

    Dave Jones and Hannes Frederic Sowa resolved an issue in the IPv6
    subsystem. Local users could cause a denial of service by using an
    AF_INET6 socket to connect to an IPv4 destination.

CVE-2013-2234

    Mathias Krause reported a memory leak in the implementation of PF_KEYv2
    sockets. Local users could gain access to sensitive kernel memory.

CVE-2013-2237

    Nicolas Dichtel reported a memory leak in the implementation of PF_KEYv2
    sockets. Local users could gain access to sensitive kernel memory.

CVE-2013-2851

    Kees Cook reported an issue in the block subsystem. Local users with
    uid 0 could gain elevated ring 0 privileges. This is only a security
    issue for certain specially configured systems.

CVE-2013-2852

    Kees Cook reported an issue in the b43 network driver for certain Broadcom
    wireless devices. Local users with uid 0 could gain elevated ring 0 
    privileges. This is only a security issue for certain specially configured
    systems.

CVE-2013-4162

    Hannes Frederic Sowa reported an issue in the IPv6 networking subsystem.
    Local users can cause a denial of service (system crash).

CVE-2013-4163

    Dave Jones reported an issue in the IPv6 networking subsystem. Local
    users can cause a denial of service (system crash).

This update also includes a fix for a regression in the Xen subsystem.

For the stable distribution (wheezy), these problems has been fixed in version
3.2.46-1+deb7u1.

The following matrix lists additional source packages that were rebuilt for
compatibility with or to take advantage of this update:

                                             Debian 7.0 (wheezy)
     user-mode-linux                         3.2-2um-1+deb7u2

We recommend that you upgrade your linux and user-mode-linux packages.

Note: Debian carefully tracks all known security issues across every
linux kernel package in all releases under active security support.
However, given the high frequency at which low-severity security
issues are discovered in the kernel and the resource requirements of
doing an update, updates for lower priority issues will normally not
be released for all kernels at the same time. Rather, they will be
released in a staggered or "leap-frog" fashion.

Further information about Debian Security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: http://www.debian.org/security/

Mailing list: debian-security-announce@lists.debian.org
