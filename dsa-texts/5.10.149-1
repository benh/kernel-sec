Package        : linux
CVE ID         : CVE-2021-4037 CVE-2022-0171 CVE-2022-1184 CVE-2022-2602
                 CVE-2022-2663 CVE-2022-3061 CVE-2022-3176 CVE-2022-3303
                 CVE-2022-20421 CVE-2022-39188 CVE-2022-39842 CVE-2022-40307
                 CVE-2022-41674 CVE-2022-42719 CVE-2022-42720 CVE-2022-42721
                 CVE-2022-42722

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2021-4037

    Christian Brauner reported that the inode_init_owner function for
    the XFS filesystem in the Linux kernel allows local users to create
    files with an unintended group ownership allowing attackers to
    escalate privileges by making a plain file executable and SGID.

CVE-2022-0171

    Mingwei Zhang reported that a cache incoherence issue in the SEV API
    in the KVM subsystem may result in denial of service.

CVE-2022-1184

    A flaw was discovered in the ext4 filesystem driver which can lead
    to a use-after-free. A local user permitted to mount arbitrary
    filesystems could exploit this to cause a denial of service (crash
    or memory corruption) or possibly for privilege escalation.

CVE-2022-2602

    A race between handling an io_uring request and the Unix socket
    garbage collector was discovered. An attacker can take advantage of
    this flaw for local privilege escalation.

CVE-2022-2663

    David Leadbeater reported flaws in the nf_conntrack_irc
    connection-tracking protocol module.  When this module is enabled
    on a firewall, an external user on the same IRC network as an
    internal user could exploit its lax parsing to open arbitrary TCP
    ports in the firewall, to reveal their public IP address, or to
    block their IRC connection at the firewall.

CVE-2022-3061

    A flaw was discovered in the i740 driver which may result in denial
    of service.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2022-3176

    A use-after-free flaw was discovered in the io_uring subsystem which
    may result in local privilege escalation to root.

CVE-2022-3303

    A race condition in the snd_pcm_oss_sync function in the sound
    subsystem in the Linux kernel due to improper locking may result in
    denial of service.

CVE-2022-20421

    A use-after-free vulnerability was discovered in the
    binder_inc_ref_for_node function in the Android binder driver. On
    systems where the binder driver is loaded, a local user could
    exploit this for privilege escalation.

CVE-2022-39188

    Jann Horn reported a race condition in the kernel's handling of
    unmapping of certain memory ranges.  When a driver created a
    memory mapping with the VM_PFNMAP flag, which many GPU drivers do,
    the memory mapping could be removed and freed before it was
    flushed from the CPU TLBs.  This could result in a page use-after-
    free.  A local user with access to such a device could exploit
    this to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.

CVE-2022-39842

    An integer overflow was discovered in the pxa3xx-gcu video driver
    which could lead to a heap out-of-bounds write.

    This driver is not enabled in Debian's official kernel
    configurations.

CVE-2022-40307

    A race condition was discovered in the EFI capsule-loader driver,
    which could lead to use-after-free.  A local user permitted to
    access this device (/dev/efi_capsule_loader) could exploit this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation.  However, this device is normally only
    accessible by the root user.

CVE-2022-41674, CVE-2022-42719, CVE-2022-42720, CVE-2022-42721, CVE-2022-42722

    Soenke Huster discovered several vulnerabilities in the mac80211
    subsystem triggered by WLAN frames which may result in denial of
    service or the execution or arbitrary code.
