Package        : linux
CVE ID         : CVE-2023-6531 CVE-2023-6622 CVE-2023-6817 CVE-2023-6931 CVE-2023-51779 CVE-2023-51780 CVE-2023-51781 CVE-2023-51782

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2023-6531

    Jann Horn discovered a use-after-free flaw due to a race condition
    problem when the unix garbage collector's deletion of a SKB races
    with unix_stream_read_generic() on the socket that the SKB is
    queued on.

CVE-2023-6622

    Xingyuan Mo discovered a flaw in the netfilter subsystem which may
    result in denial of service or privilege escalation for a user with
    the CAP_NET_ADMIN capability in any user or network namespace.

CVE-2023-6817

    Xingyuan Mo discovered that a use-after-free in Netfilter's
    implementation of PIPAPO (PIle PAcket POlicies) may result in denial
    of service or potential local privilege escalation for a user with
    the CAP_NET_ADMIN capability in any user or network namespace.

CVE-2023-6931

    Budimir Markovic reported a heap out-of-bounds write vulnerability
    in the Linux kernel's Performance Events system which may result in
    denial of service or privilege escalation.

CVE-2023-51779

    It was discovered that a race condition in the Bluetooth subsystem
    in the bt_sock_ioctl handling may lead to a use-after-free.

CVE-2023-51780

    It was discovered that a race condition in the ATM (Asynchronous
    Transfer Mode) subsystem may lead to a use-after-free.

CVE-2023-51781

    It was discovered that a race condition in the Appletalk subsystem
    may lead to a use-after-free.

CVE-2023-51782

    It was discovered that a race condition in the Amateur Radio X.25
    PLP (Rose) support may lead to a use-after-free.

